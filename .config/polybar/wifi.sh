state=$( cat /sys/class/net/wlp2s0/operstate )
ssid=$( nmcli -t -f active,ssid dev wifi | egrep '^yes' | cut -d ':' -f2 )

if [ $state = "dormant" ]
then
  echo 
elif [ $state = "up" ]
then
  echo $ssid
else
  echo 
fi
