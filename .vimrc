set number
set tabstop=2
set autoread
set shiftwidth=4
set expandtab
set autoindent
set  rtp+=~/.local/lib/python2.7/site-packages/powerline/bindings/vim/
set laststatus=2
execute pathogen#infect()
call pathogen#helptags()
" autocmd vimenter * NERDTree
set t_Co=256
set t_ut=
colorscheme wal
